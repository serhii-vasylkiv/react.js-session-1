import React from 'react';
import Task from "./Task.jsx";

class TaskList extends React.Component {
    constructor (props) {
        super(props);
    }

    render() {
        var _tasks = this.props.tasks.map(function(text, index) {
                return <Task text={text}/>
            }),
            _count = this.props.tasks.length;

        return (
            <div>
                {_tasks}
                <p>Total tasks: {_count}</p>
            </div>
        );
    }
}

export default TaskList;
