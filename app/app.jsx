import React from "react";
import Task from "./components/Task.jsx";
import TaskList from "./components/TaskList.jsx";

class App extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div>
        <h2>Tasks</h2>
        <TaskList tasks={this.props.tasks}/>
      </div>
    )
  }
}

React.render(<App tasks={[{text: 'Task 1'},{text: 'Task 2'},{text: 'Task 3'}]} />, document.getElementById("content"));
